# podman kill -a
# buildah rm --all && podman system prune --all --force && podman rmi --all
cd /scm/s2i-nodejs-container

#podman build . --force-rm -t test
#rm -Rf build
#mkdir -p build
#cp -R docker/test/src build/src
#cd build
export S2I_IMAGE="registry.gitlab.com/xrow-public/s2i-nodejs-container/node:14"
export S2I_SRC_DIR="docker/test"
sh ../ci-tools/scripts/node/s2i
podman build --format=docker --build-arg "BASE=$S2I_IMAGE" --force-rm -t node $S2I_SRC_DIR
podman tag node registry.gitlab.com/xrow-public/s2i-nodejs-container/test:latest
podman push registry.gitlab.com/xrow-public/s2i-nodejs-container/test:latest
podman run -it node
export HELM_EXPERIMENTAL_OCI=1
helm pull oci://registry.gitlab.com/xrow-public/repository/developer-operator --version 1.0.11

podman run -it registry.gitlab.com/siedle.de/siedle-frontend-framework:74800cedd2cabea9dfdd2efc461803ec4b0922ea bash