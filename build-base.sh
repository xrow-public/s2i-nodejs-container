#!/bin/bash

set -e

while getopts t: flag
do
    case "${flag}" in
        t) TARGET=${OPTARG};;
    esac
done

repo="s2i-nodejs-container"

yum install -y podman-docker go make

rm -Rf s2i-nodejs-container
export PATH=$PATH:$(go env GOPATH)/bin
go get github.com/cpuguy83/go-md2man
git clone --recursive https://github.com/sclorg/$repo.git
sed -i 's/f33/f34/g' $repo/14/Dockerfile.fedora

cd $repo
echo "Start Build"
make build --trace -k -C $PWD TARGET=$TARGET VERSIONS=14;
echo "End Build"
DIGEST=$(podman images | awk '{print $3}' | awk 'NR==2')
echo "Push Build registry.gitlab.com/xrow-public/s2i-nodejs-container:base-$TARGET"
podman login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
podman tag $DIGEST registry.gitlab.com/xrow-public/s2i-nodejs-container:base-$TARGET
podman push registry.gitlab.com/xrow-public/s2i-nodejs-container:base-$TARGET
cd ..
rm -Rf $repo
echo "Done Build"

